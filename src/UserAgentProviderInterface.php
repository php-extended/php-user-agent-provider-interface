<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-provider-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use Iterator;
use Stringable;

/**
 * UserAgentProviderInterface interface file.
 * 
 * This interface specifies which is a provider for user agents.
 * 
 * @author Anastaszor
 */
interface UserAgentProviderInterface extends Stringable
{
	
	/**
	 * Searches for user agents that matches with the given query. The
	 * pagination for the same query begins at 1, 0 and negative values are
	 * ignored and threated as 1. The returned iterator may contain no results.
	 * 
	 * @param UserAgentQueryInterface $query
	 * @param integer $page
	 * @return Iterator<UserAgentInterface>
	 */
	public function search(UserAgentQueryInterface $query, int $page = 1) : Iterator;
	
	/**
	 * Gets the next user agent. If there are no more user agents available,
	 * null may be returned. The behavior of the provider differs whether it
	 * is a fixed user agent (which always return the same value and never
	 * runs out of values), a rolling user agent (which returns a value amongst
	 * a set of fixed values and never runs out of values), or a catalog (which
	 * iterates over all its known user agents only once). There may be other
	 * behaviors that are not described here.
	 * 
	 * @return ?UserAgentInterface
	 */
	public function getNextUserAgent() : ?UserAgentInterface;
	
}
