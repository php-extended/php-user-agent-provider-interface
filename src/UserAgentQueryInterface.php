<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-provider-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use Stringable;

/**
 * UserAgentQueryInterface interface file.
 * 
 * This represents the paramters to do a query for user agent iteration.
 * 
 * @author Anastaszor
 */
interface UserAgentQueryInterface extends Stringable
{
	
	/**
	 * Sets the search token for the brand.
	 * 
	 * @param string $name
	 * @return UserAgentQueryInterface
	 */
	public function setBrandName(string $name) : UserAgentQueryInterface;
	
	/**
	 * Gets the search token for the brand.
	 *
	 * @return ?string
	 */
	public function getBrandName() : ?string;
	
	/**
	 * Sets the search token for the device.
	 * 
	 * @param string $name
	 * @return UserAgentQueryInterface
	 */
	public function setDeviceName(string $name) : UserAgentQueryInterface;
	
	/**
	 * Gets the search token for the device.
	 *
	 * @return ?string
	 */
	public function getDeviceName() : ?string;
	
	/**
	 * Sets the search token for the device type.
	 * 
	 * @param string $name
	 * @return UserAgentQueryInterface
	 */
	public function setDeviceTypeName(string $name) : UserAgentQueryInterface;
	
	/**
	 * Gets the search token for the device type.
	 *
	 * @return ?string
	 */
	public function getDeviceTypeName() : ?string;
	
	/**
	 * Sets the search token for the operating system family.
	 * 
	 * @param string $name
	 * @return UserAgentQueryInterface
	 */
	public function setOperatingSystemFamilyName(string $name) : UserAgentQueryInterface;
	
	/**
	 * Gets the search token for the operating system family.
	 *
	 * @return ?string
	 */
	public function getOperatingSystemFamilyName() : ?string;
	
	/**
	 * Sets the search token for the operating system.
	 * 
	 * @param string $name
	 * @return UserAgentQueryInterface
	 */
	public function setOperatingSystemName(string $name) : UserAgentQueryInterface;
	
	/**
	 * Gets the search token for the operating system.
	 *
	 * @return ?string
	 */
	public function getOperatingSystemName() : ?string;
	
	/**
	 * Sets the search token for the rendering engine family.
	 * 
	 * @param string $name
	 * @return UserAgentQueryInterface
	 */
	public function setRenderingEngineFamilyName(string $name) : UserAgentQueryInterface;
	
	/**
	 * Gets the search token for the rendering engine family.
	 *
	 * @return ?string
	 */
	public function getRenderingEngineFamilyName() : ?string;
	
	/**
	 * Sets the search token for the rendering engine.
	 * 
	 * @param string $name
	 * @return UserAgentQueryInterface
	 */
	public function setRenderingEngineName(string $name) : UserAgentQueryInterface;
	
	/**
	 * Gets the search token for the rendering engine.
	 *
	 * @return ?string
	 */
	public function getRenderingEngineName() : ?string;
	
	/**
	 * Sets the search token for the user agent type.
	 * 
	 * @param string $name
	 * @return UserAgentQueryInterface
	 */
	public function setUserAgentTypeName(string $name) : UserAgentQueryInterface;
	
	/**
	 * Gets the search token for the user agent type.
	 * 
	 * @return ?string
	 */
	public function getUserAgentTypeName() : ?string;
	
	/**
	 * Sets the maximum levenshtein distance to the exact matches (to do fuzzy
	 * searches). Zero is exact match, and negative is treated as zero.
	 * 
	 * @param integer $distance
	 * @return UserAgentQueryInterface
	 */
	public function setLevenshteinDistanceMax(int $distance) : UserAgentQueryInterface;
	
	/**
	 * Gets the maximum levenshtein distance to the exact matches (to do fuzzy
	 * searches). Zero is exact match.
	 * 
	 * @return integer
	 */
	public function getLevenshteinDistanceMax() : int;
	
}
